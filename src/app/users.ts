// Handle app logic
import * as db from '../db';
import { Users } from '../entity/Users';

export type RegisterUserParams = {
    userName: string,
    firstName: string,
    lastName: string,
    telNo: string,
    email: string,
    birthDate: Date
}
export const registerUser = async (params: RegisterUserParams) => {
    // TODO: Validate params
    const user = new Users();
    user.userName = params.userName;
    user.firstName = params.firstName;
    user.lastName = params.lastName;
    user.telNo = params.telNo;
    user.email = params.email;
    user.birthDate = params.birthDate;
    return db.createUser(user);
}