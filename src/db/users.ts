import { Users } from "../entity/Users";
import { getConnection } from "typeorm"

export const createUser = async (param: Users): Promise<Users> => {
    // TODO: Validate params
    return getConnection().getRepository(Users).save(param);
}

export const getAllUsers = async (): Promise<Users[]> => {
    return getConnection().getRepository(Users).find();
}