// import { createLogger, format, transports } from 'winston';
import * as winston from 'winston';

const levels = {
    error: 0,
    warn: 1,
    info: 2,
    http: 3,
    debug: 4,
}

const level = () => {
    const env = process.env.NODE_ENV || 'development'
    const isDevelopment = env === 'development'
    return isDevelopment ? 'debug' : 'warn'
}

const colors = {
    error: 'red',
    warn: 'yellow',
    info: 'green',
    http: 'magenta',
    debug: 'white',
}

winston.addColors(colors)

const format = winston.format.combine(
    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
    winston.format.colorize({ all: true }),
    winston.format.printf(
        (info) => `${info.timestamp} ${info.level}: ${info.message}`,
    ),
)

const transports = [
    new winston.transports.Console(),
    new winston.transports.File({
        filename: 'logs/error.log',
        level: 'error',
    }),
    new winston.transports.File({ filename: 'logs/all.log' }),
]

const Logger = winston.createLogger({
    level: level(),
    levels,
    format,
    transports,
})

export default Logger

// export const logger = createLogger({
//     transports:
//         new transports.File({
//             filename: 'logs/server.log',
//             format: format.combine(
//                 format.colorize(),
//                 format.timestamp({ format: 'MMM-DD-YYYY HH:mm:ss' }),
//                 format.align(),
//                 format.printf(info => `${info.level}: ${[info.timestamp]}: ${info.message}`),
//             ),

//         }),
//     // format: format.combine(
//     //     format.colorize(),
//     //     format.timestamp({ format: 'MMM-DD-YYYY HH:mm:ss' }),
//     //     format.align(),
//     //     format.printf(info => `${info.level}: ${[info.timestamp]}: ${info.message}`),
//     // ),
// });

