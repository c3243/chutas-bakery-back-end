import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { Users } from "../entity/Users";

export class UsersController {

    private usersRepository = getRepository(Users);

    async register(userName: string) {
        const user = new Users();
        user.firstName = "Zeze"
        user.userName = userName
        return this.usersRepository.save(user);
    }

    async all(request: Request, response: Response, next: NextFunction) {
        return this.usersRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.usersRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        console.log(request.body);
        return this.usersRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.usersRepository.findOne(request.params.id);
        await this.usersRepository.remove(userToRemove);
    }

}