import { UsersController } from "./controller/UsersController";


export const Routes = [{
    method: "get",
    route: "/users",
    controller: UsersController,
    middlewares: [],
    action: "all"
}, {
    method: "post",
    route: "/register",
    controller: UsersController,
    middlewares: [],
    action: "save"
}]
// export const Routes = [{
//     method: "get",
//     route: "/users",
//     controller: UserController,
//     action: "all"
// }, {
//     method: "get",
//     route: "/users/:id",
//     controller: UserController,
//     action: "one"
// }, {
//     method: "post",
//     route: "/users",
//     controller: UserController,
//     action: "save"
// }, {
//     method: "delete",
//     route: "/users/:id",
//     controller: UserController,
//     action: "remove"
// }];