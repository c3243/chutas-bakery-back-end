import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
// import { Routes } from "./routes";
import routes from './routes';
import logger from "./utils/logger";
import morganMiddleware from "./config/morganMiddleware";

createConnection().then(async connection => {

    // create express app
    const app = express();
    app.use(bodyParser.json());

    // HTTP Log
    // var morgan = require('morgan')
    app.use(morganMiddleware);

    // register express routes from defined application routes
    // Routes.forEach(route => {
    //     (app as any)[route.method](route.route, route.middlewares, (req: Request, res: Response, next: Function) => {
    //         const result = (new (route.controller as any))[route.action](req, res, next);
    //         if (result instanceof Promise) {
    //             result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

    //         } else if (result !== null && result !== undefined) {
    //             res.json(result);
    //         }
    //     });
    // });


    // setup express app here

    // Create reqLogger
    const reqLogger = (req: Request, res: Response, next: Function) => {
        logger.info(req.body);
        next();
    }
    app.use(reqLogger);

    // Register route
    app.use('/api', routes);

    // Capture 500 errors
    app.use((err, req, res, next) => {
        // res.status(500).send('Could not perform the calculation!');
        res.status(500).json({ error: err.toString() });
        logger.error(`${err.status || 500} - ${res.statusMessage} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
    })

    // Capture 404 erors
    app.use((req: Request, res: Response, next: Function) => {
        res.status(404).send("NOT FOUND");
        logger.error(`400 || ${res.statusMessage} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
    })

    // start express server
    app.listen(3000);

    // // insert new users for test
    // await connection.manager.save(connection.manager.create(User, {
    //     firstName: "Timber",
    //     lastName: "Saw",
    //     age: 27
    // }));
    // await connection.manager.save(connection.manager.create(User, {
    //     firstName: "Phantom",
    //     lastName: "Assassin",
    //     age: 24
    // }));
    // await connection.manager.save(connection.manager.create(Users, {
    //     firstName: "Phantom",
    //     lastName: "Assassin",
    //     userName: "satasuk012",
    //     birthDate: new Date(),
    // }));

    logger.info("Express server has started on port 3000. Open http://localhost:3000/users to see results");

}).catch(error => logger.error(error));
