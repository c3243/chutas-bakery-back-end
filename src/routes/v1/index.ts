import { Router } from 'express';
import user from './users';

const router = Router();
router.use('/users', user);

export default router;