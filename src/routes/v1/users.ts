import { getAllUsers } from '../../db/users';
import { Router, Request, Response } from 'express';
import { registerUser } from '../../app/users';

const router = Router();

router.post('/register', async (request: Request, response: Response) => {
    try {
        // const res = await register(request.body.userName);
        const res = await registerUser(request.body);
        response.json(res);
    } catch (err) {
        response.status(500).send({ error: String(err) });
    }
    return;
});
router.get('/all', async (request: Request, response: Response) => {
    const res = await getAllUsers();
    response.json(res);
    // throw new Error("HEHE");
    // response.status(500)
    return;
});

export default router;