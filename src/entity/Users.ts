
import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn } from "typeorm";

@Entity()
export class Users {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    userName: string;

    @Column()
    firstName: string;

    @Column({ nullable: true })
    lastName: string;

    @Column({ nullable: true })
    telNo: string;

    @Column({ nullable: true })
    email: string;

    @Column({ type: 'timestamptz', nullable: true })
    birthDate: Date;

    @Column({ default: false })
    emailValidated: boolean;

    @CreateDateColumn({ type: 'timestamptz' })
    createTime: Date;

    @UpdateDateColumn({ type: 'timestamptz' })
    updateTime: Date;
}
