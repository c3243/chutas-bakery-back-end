#!/bin/bash

trap killgroup SIGINT

killgroup(){
  kill 0
}
docker stop $(docker ps -a -q)

docker run --rm -p 5432:5432 -e TZ=Asia/Bangkok -e POSTGRES_PASSWORD=postgres --name chutas_postgres postgres:12-alpine

wait
